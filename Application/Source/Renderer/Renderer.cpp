#include "Renderer.hpp"

#include <iostream>
#include <thread>


RendererC::RendererC( void )
{
    std::cout << "RendererC::RendererC( void )" << std::endl;
}


RendererC::~RendererC( void )
{
    std::cout << "RendererC::~RendererC( void )" << std::endl;
}


void RendererC::Render( void )
{
    std::cout << "RendererC::Render( void )" << std::endl;
    while( currentlyOpenedScreen->IsScreenActive() && mainWindow->isOpen() )
    {
        mainWindow->setActive( true );
//        mainWindow->clear();

        DrawScreen();

        const auto& widgetsNumber = currentlyOpenedScreen->GetNumberOfWidgets();
        for( size_t it = 0; it < widgetsNumber; ++it )
            mainWindow->draw( currentlyOpenedScreen->GetWidget( it ) );

        mainWindow->display();
        std::this_thread::sleep_for( std::chrono::milliseconds( 1000/60 ) );
        mainWindow->setActive( false );
    }
}


void RendererC::RegisterWindowHandle( std::shared_ptr<MainWindowC> mainWindow )
{
    std::cout << "RendererC::RegisterWindowHandle( " << mainWindow.get() << " )" << std::endl;
    this->mainWindow = mainWindow;
}


void RendererC::OpenScreen( std::shared_ptr<ScreensInfo::Screen> screen )
{
    std::cout << "RendererC::OpenScreen( " << screen.get() << " )" << std::endl;
    currentlyOpenedScreen = screen;
    currentlyOpenedScreen->SetActive( true );
}


void RendererC::DrawScreen( void )
{
    mainWindow->draw( currentlyOpenedScreen->background );
}
