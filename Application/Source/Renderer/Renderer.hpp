#pragma once

#include "Window/MainWindow.hpp"
#include "Screens/Screens.hpp"

#include <Window.hpp>

#include <memory>
#include <vector>


class RendererC
{
public:
    RendererC( void );

    ~RendererC( void );

    void Render( void );

    void RegisterWindowHandle( std::shared_ptr<MainWindowC> mainWindow );

    void OpenScreen( std::shared_ptr<ScreensInfo::Screen> screen );

private:
    void DrawScreen( void );

    std::shared_ptr<MainWindowC> mainWindow;

    std::shared_ptr<ScreensInfo::Screen> currentlyOpenedScreen;
};
