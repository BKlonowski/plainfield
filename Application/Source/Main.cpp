#include <iostream>
#include <thread>
#include <array>

#include "Window/MainWindow.hpp"
#include "Renderer/Renderer.hpp"
#include "Screens/Screens.hpp"
#include "Screens/Menu/MenuC.hpp"
#include "Screens/Help/HelpScreenC.hpp"
#include "Screens/Game/GameScreenC.hpp"


int main( void )
{
    std::cout << "PlainField" << std::endl;

    std::shared_ptr<MainWindowC> mainWindow = std::make_shared<MainWindowC>();
    mainWindow->setActive( false );

    std::array<std::shared_ptr<ScreensInfo::Screen>, ScreensInfo::None> screens = {
        std::make_shared<MenuC>(),
        std::make_shared<HelpScreenC>(),
        std::make_shared<GameScreenC>(),
        std::make_shared<EquipmentC>()
    };

    // Render section and its thread
    RendererC render;
    render.RegisterWindowHandle( mainWindow );
    std::thread renderer( [&]()->void
    {
        using namespace std::chrono_literals;
        ScreensInfo::currentScreen = ScreensInfo::Menu;
        while( mainWindow->isOpen() )
        {
            render.OpenScreen( screens.at( ScreensInfo::currentScreen ) );
            render.Render();
            std::this_thread::sleep_for( 1ms );
        }
    } );


    // Control section as the main thread
    while( mainWindow->isOpen() )
    {
        sf::Event event;
        while( mainWindow->pollEvent( event ) )
        {
            if( event.type == sf::Event::Closed )
                mainWindow->close();
            else
                screens.at( ScreensInfo::currentScreen )->ScreenControlStateMachine( std::move( event ) );
        }
    }


    renderer.join();
    std::cin.get();
    return 0;
}
