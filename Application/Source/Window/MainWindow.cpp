#include "MainWindow.hpp"
#include "GameSettings/Settings.hpp"

#include <iostream>
#include <string>


MainWindowC::MainWindowC( void )
{
    std::cout << "MainWindowC::MainWindowC( void )" << std::endl;

    create( sf::VideoMode( sf::VideoMode::getDesktopMode().width, sf::VideoMode::getDesktopMode().height ),
            sf::String("ColoRing"),
            sf::Style::Fullscreen );
}


MainWindowC::~MainWindowC( void )
{
    std::cout << "MainWindowC::~MainWindowC( void )" << std::endl;
}
