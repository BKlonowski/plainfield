#pragma once

#include "Screens\Screens.hpp"

#include <array>


class MenuC : public ScreensInfo::Screen
{
public:
    MenuC( void );

    ~MenuC( void );

    void ScreenControlStateMachine( sf::Event&& event ) override;


private:
    void KeyPressedStateMachine( sf::Event&& event );

    void KeyReleasedStateMachine( void );

    const ScreensInfo::Tag OptionToScreenTag( void );

    std::shared_ptr<sf::Texture> arrowTexture;

    // Arrow positions assigned to the options widgets
    static constexpr float arrowWidgetPositionX = 150;
    enum MenuOptions
    {
        START,
        HELP,
        QUIT,

        MAX_POSITION
    };
    std::array<float, MAX_POSITION> widgetsYPositions =
    {
        110,
        285,
        435
    };
    MenuOptions currentOptionSelected;
};
