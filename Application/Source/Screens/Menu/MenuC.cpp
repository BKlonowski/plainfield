#include "MenuC.hpp"

#include <iostream>


MenuC::MenuC( void ) : arrowTexture(std::make_shared<sf::Texture>()), currentOptionSelected(START)
{
    std::cout << "MenuC::MenuC()" << std::endl;
    backgroundImage.loadFromFile( "Dependencies/Textures/TitleScreen.png" );
    background.setTexture( &backgroundImage );

    if( arrowTexture->loadFromFile( "Dependencies/Textures/ArrowMenu.png" ) )
    {
        AddWidget( std::move( std::make_unique<sf::RectangleShape>(
            sf::RectangleShape(sf::Vector2f(Settings::menuArrowSizeInPixels.first, Settings::menuArrowSizeInPixels.second))) ) );
        AddWidgetTexture( arrowTexture.get() );
        GetWidget().setPosition( arrowWidgetPositionX, widgetsYPositions.at(START) );
    }
    else
    {
        std::cout << "ERROR:  Arrow pointer texture could not be loaded!" << std::endl;
    }
}


MenuC::~MenuC( void )
{
    std::cout << "MenuC::~MenuC()" << std::endl;
}


void MenuC::ScreenControlStateMachine( sf::Event&& event )
{
    std::cout << "MenuC::ScreenControlStateMachine( " << &event << " )" << std::endl;
    if( event.type == sf::Event::KeyPressed )
    {
        KeyPressedStateMachine( std::move( event ) );
    }
    else if( event.type == sf::Event::KeyReleased )
    {
        KeyReleasedStateMachine();
    }
}


void MenuC::KeyPressedStateMachine( sf::Event&& event )
{
    std::cout << "MenuC::KeyPressedStateMachine()" << std::endl;

    switch( event.key.code )
    {

    case sf::Keyboard::Up:
        std::cout << "case sf::Keyboard::Up" << std::endl;
        if( currentOptionSelected > START )
            currentOptionSelected = static_cast<MenuOptions>(static_cast<int>(currentOptionSelected) - 1);
        break;

    case sf::Keyboard::Down:
        std::cout << "case sf::Keyboard::Down" << std::endl;
        if( currentOptionSelected < QUIT )
            currentOptionSelected = static_cast<MenuOptions>(static_cast<int>(currentOptionSelected) + 1);
        break;

    case sf::Keyboard::Return:
        std::cout << "case sf::Keyboard::Return" << std::endl;
        ScreensInfo::currentScreen = OptionToScreenTag();
        SetActive( false );
        break;

    default:
        std::cout << "ERROR:  Unknown keyboard keyPressed!" << std::endl;
        break;
    }
}


void MenuC::KeyReleasedStateMachine( void )
{
    std::cout << "MenuC::KeyReleasedStateMachine()" << std::endl;
    GetWidget().setPosition( arrowWidgetPositionX, widgetsYPositions.at( currentOptionSelected ) );
}


const ScreensInfo::Tag MenuC::OptionToScreenTag( void )
{
    switch( currentOptionSelected )
    {
    case START:
        return ScreensInfo::Game;
        break;
    case HELP:
        return ScreensInfo::Help;
        break;
    case QUIT:
        return ScreensInfo::None;
        break;
    default:
        std::cout << "ERROR:  Unrecognized option selected!" << std::endl;
        return ScreensInfo::None;
        break;
    }
}
