#include "Screens.hpp"

#include <iostream>



namespace ScreensInfo
{
    Tag currentScreen = None;
}


ScreensInfo::Screen::Screen( void ) : isActive(false)
{
    std::cout << "Screen::Screen()" << std::endl;
    background.setSize( sf::Vector2f(
        static_cast<float>(sf::VideoMode::getDesktopMode().width),
        static_cast<float>(sf::VideoMode::getDesktopMode().height)
    ) );
    background.setFillColor( sf::Color::White );
}


ScreensInfo::Screen::~Screen( void )
{
    std::cout << "Screen::~Screen()" << std::endl;
}


void ScreensInfo::Screen::AddWidget( std::shared_ptr<sf::Shape>&& widget )
{
    std::cout << "Screen::AddWidget( " << &widget << " )" << std::endl;
    screenWidgets.push_back( std::move( widget ) );
}


void ScreensInfo::Screen::AddWidgetTexture( sf::Texture* texture, const int widgetIndex )
{
    std::cout << "Screen::AddWidgetTexture( " << texture << ", " << widgetIndex << " )" << std::endl;
    screenWidgets.at( widgetIndex )->setTexture( texture );
}


sf::Shape& ScreensInfo::Screen::GetWidget( const int widgetIndex )
{
    return *screenWidgets.at( widgetIndex );
}


void ScreensInfo::SwitchToScreen( const ScreensInfo::Tag newScreen )
{
    if( newScreen != None )
        currentScreen = newScreen;
    else
        std::cout << "ERROR: Wrong screen selected to switch!" << std::endl;
}

