#include "Box.hpp"
#include "GameSettings/Settings.hpp"

#include <iostream>

std::shared_ptr<BoxC> BoxC::GetInstance( void )
{
    static std::shared_ptr<BoxC> box(new BoxC);
    return box;
}


void BoxC::SetPosition( std::pair<float, float> position )
{
    setPosition( position.first, position.second );
}


std::pair<float, float> BoxC::GetPosition( void ) const
{
    return { getPosition().x, getPosition().y };
}


BoxC::BoxC( void ) : sf::RectangleShape( sf::Vector2f( Settings::equipmentBoxSize.first, Settings::equipmentBoxSize.second ) )
{
    texture.loadFromFile( "Dependencies/Textures/Equipment.png" );
    setTexture( &texture );
}
