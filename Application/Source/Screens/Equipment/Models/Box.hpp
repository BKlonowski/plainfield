#pragma once

#include "Graphics/RectangleShape.hpp"
#include "Graphics/Texture.hpp"

class BoxC : public sf::RectangleShape
{
public:
    static std::shared_ptr<BoxC> GetInstance( void );

    void operator=( const BoxC& box ) = delete;
    BoxC( BoxC const& ) = delete;

    void SetPosition( std::pair<float, float> position );
    std::pair<float, float> GetPosition( void ) const;


private:
    BoxC( void );
    sf::Texture texture;
};
