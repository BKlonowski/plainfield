#pragma once

#include "Screens/Screens.hpp"

class EquipmentC : public ScreensInfo::Screen
{
public:
    EquipmentC( void );
    ~EquipmentC( void );

    void ScreenControlStateMachine( sf::Event&& event ) override;

    const bool IsOpened( void ) const;
    void Open( void );

private:
    bool isOpened;
};
