#include "Equipment.hpp"
#include "Screens/Equipment/Models/Box.hpp"


EquipmentC::EquipmentC( void )
{
    backgroundImage.loadFromFile( "Dependencies/Textures/Equipment_Background.png" );
    background.setTexture( &backgroundImage );

    AddWidget( BoxC::GetInstance() );
}


EquipmentC::~EquipmentC( void )
{
}


void EquipmentC::ScreenControlStateMachine( sf::Event&& event )
{
    if( event.type == sf::Event::KeyPressed )
    {
        switch( event.key.code )
        {
        case sf::Keyboard::Key::E:
        case sf::Keyboard::Key::Escape:
            SetActive( false );
            ScreensInfo::currentScreen = ScreensInfo::Tag::Game;
            break;
        default:
            break;
        }
    }
    else if( event.type == sf::Event::KeyReleased )
    {
    }
}


const bool EquipmentC::IsOpened( void ) const
{
    return isOpened;
}

void EquipmentC::Open( void )
{
    isOpened = true;
}
