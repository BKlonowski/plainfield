#pragma once

#include "Graphics.hpp"
#include "GameSettings/Settings.hpp"

#include <string>
#include <memory>


namespace ScreensInfo
{
    enum Tag
    {
        Menu,
        Help,
        Game,
        Equipment,

        // Should not be used - indicates the error
        None
    };

    class Screen
    {
    public:
        Screen( void );

        virtual ~Screen( void );

        virtual void ScreenControlStateMachine( sf::Event&& event ) = 0;

        void AddWidget( std::shared_ptr<sf::Shape>&& widget );

        void AddWidgetTexture( sf::Texture* texture, const int widgetIndex = 0 );

        sf::Shape& GetWidget( const int widgetIndex = 0 );

        const size_t GetNumberOfWidgets( void )
        {
            return screenWidgets.size();
        }

        const bool IsScreenActive( void )
        {
            return isActive;
        }

        virtual void SetActive( const bool isActive )
        {
            this->isActive = isActive;
        }

        sf::RectangleShape background;
        sf::Texture backgroundImage;

    protected:
        bool isActive;

    private:
        std::vector<std::shared_ptr<sf::Shape>> screenWidgets;
    };

    void SwitchToScreen( const Tag newScreen );

    extern Tag currentScreen;
}
