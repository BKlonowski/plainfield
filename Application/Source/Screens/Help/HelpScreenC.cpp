#include "HelpScreenC.hpp"

#include <iostream>


HelpScreenC::HelpScreenC(void)
{
    std::cout << "HelpScreenC::HelpScreenC()" << std::endl;
    backgroundImage.loadFromFile( "Dependencies/Textures/hlp00.jpg" );
    background.setTexture( &backgroundImage );
}


HelpScreenC::~HelpScreenC(void)
{
}


void HelpScreenC::ScreenControlStateMachine( sf::Event&& event )
{
    if( event.type == sf::Event::KeyPressed )
    {
        KeyPressedStateMachine( std::move( event ) );
    }
    else if( event.type == sf::Event::KeyReleased )
    {
        KeyReleasedStateMachine();
    }
}


void HelpScreenC::KeyPressedStateMachine( sf::Event&& event )
{
    std::cout << "HelpScreenC::KeyPressedStateMachine()" << std::endl;
    switch( event.key.code )
    {
    // Both keys are returning to and only to the menu screen
    case sf::Keyboard::Escape:
    case sf::Keyboard::Return:
        ScreensInfo::currentScreen = ScreensInfo::Menu;
        SetActive( false );
        break;

    default:
        std::cout << "ERROR:  Unknown event!" << std::endl;
        break;
    }
}


void HelpScreenC::KeyReleasedStateMachine( void )
{
    std::cout << "HelpScreenC::KeyReleasedStateMachine()" << std::endl;
}
