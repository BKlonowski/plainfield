#pragma once

#include "Screens/Screens.hpp"


class HelpScreenC : public ScreensInfo::Screen
{
public:
    HelpScreenC(void);

    ~HelpScreenC(void);

    void ScreenControlStateMachine( sf::Event&& event ) override;


private:
    void KeyPressedStateMachine( sf::Event&& event );

    void KeyReleasedStateMachine( void );
};

