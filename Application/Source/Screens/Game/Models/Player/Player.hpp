#pragma once

#include <Graphics.hpp>

#include "Screens/Equipment/Equipment.hpp"

#include <thread>
#include <array>


namespace Movement
{
    enum Directions
    {
        DOWN,
        LEFT,
        UP,
        RIGHT,

        // Should not be used. Indicates an error
        MAX
    };
}


class PlayerC : public sf::RectangleShape
{
public:
    PlayerC( void );
    ~PlayerC( void );

    // Movement of the player
    void MoveUp( void );
    void MoveDown( void );
    void MoveLeft( void );
    void MoveRight( void );
    void Stop( Movement::Directions&& stoppedDirection );
    void Stop( void );

    void MakeAction( void );

    void Rotate( const float angle );
    const float GetAngle( void ) const;

    const bool IsMoving( void ) const;

private:
    // Information about the player during the game
    unsigned int score;
    bool isAlive;

    std::array<bool, Movement::Directions::MAX> movementStatus =
    {
        false,
        false,
        false,
        false
    };

    std::thread movement;

    std::unique_ptr<sf::Texture> playerTexture;
    void AnimateMovement( void );

    const float playerStepSpeed = 1.f;

    void Move( Movement::Directions&& direction );
    const bool IsInBorders( std::pair<float, float> requestedPosition ) const;

    const bool IsInVerticalBorder( const float requestedYPosition ) const;
    const bool IsInHorizontalBorder( const float requestedXPosition ) const;

    const float DirectionToAngle( Movement::Directions&& direction ) const;
};
