#include "Player.hpp"
#include "GameSettings/Settings.hpp"

#include <iostream>



PlayerC::PlayerC( void ) : RectangleShape(sf::Vector2f(Settings::playerSizeInPixels.first, Settings::playerSizeInPixels.first )), score(0)
{
    std::cout << "PlayerC::PlayerC()" << std::endl;

    playerTexture = std::make_unique<sf::Texture>();
    playerTexture->loadFromFile( "Dependencies/Textures/Player_stop.png" );
    playerTexture->setSmooth( true );
    setTexture( playerTexture.get() );
    setOrigin( std::move( sf::Vector2f( Settings::playerSizeInPixels.first/2, Settings::playerSizeInPixels.second/2 ) ) );

    isAlive = true;
    movement = std::move( std::thread( [this]()->void
    {
        while( isAlive )
        {
            for( size_t it = 0; it < movementStatus.size(); ++it )
            {
                if( movementStatus.at( it ) )
                {
                    Move( static_cast<Movement::Directions>(it) );
                    setRotation( DirectionToAngle( static_cast<Movement::Directions>(it) ) );
                }
            }
            std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
            AnimateMovement();
        }
    } ) );
}


PlayerC::~PlayerC( void )
{
    std::cout << "PlayerC::~PlayerC()" << std::endl;
    isAlive = false;
    movement.join();
}


void PlayerC::MoveUp( void )
{
    std::cout << "void PlayerC::MoveUp()" << std::endl;
    const auto currentPosition = getPosition();
    const auto requestedYPosition = currentPosition.y - playerStepSpeed - Settings::gameFieldBorderWidth;
    movementStatus.at( Movement::Directions::UP ) = IsInBorders( { currentPosition.x, requestedYPosition } );
}


void PlayerC::MoveDown( void )
{
    std::cout << "void PlayerC::MoveDown()" << std::endl;
    const auto currentPosition = getPosition();
    const auto requestedYPosition = currentPosition.y + playerStepSpeed + Settings::gameFieldBorderWidth;
    movementStatus.at( Movement::Directions::DOWN ) = IsInBorders( { currentPosition.x, requestedYPosition } );
}


void PlayerC::MoveLeft( void )
{
    std::cout << "void PlayerC::MoveLeft()" << std::endl;
    const auto currentPosition = getPosition();
    const auto requestedXPosition = currentPosition.x - playerStepSpeed - Settings::gameFieldBorderWidth;
    movementStatus.at( Movement::Directions::LEFT ) = IsInBorders( { requestedXPosition, currentPosition.y } );
}


void PlayerC::MoveRight( void )
{
    std::cout << "void PlayerC::MoveRight()" << std::endl;
    const auto currentPosition = getPosition();
    const auto requestedXPosition = currentPosition.x + playerStepSpeed + Settings::gameFieldBorderWidth;
    movementStatus.at( Movement::Directions::RIGHT ) = IsInBorders( { requestedXPosition, currentPosition.y } );
}


void PlayerC::Stop( Movement::Directions&& stoppedDirection )
{
    movementStatus[stoppedDirection] = false;
}


void PlayerC::Stop()
{
    for( auto& it : movementStatus )
    {
        it = false;
    }
}


void PlayerC::MakeAction( void )
{

}


void PlayerC::Move( Movement::Directions&& direction )
{
    auto newPosition = getPosition();

    switch( direction )
    {
    case Movement::Directions::LEFT:
        newPosition.x -= playerStepSpeed;
        break;
    case Movement::Directions::UP:
        newPosition.y -= playerStepSpeed;
        break;
    case Movement::Directions::RIGHT:
        newPosition.x += playerStepSpeed;
        break;
    case Movement::Directions::DOWN:
        newPosition.y += playerStepSpeed;
        break;
    default:
        std::cout << "ERROR:  Unknown movement direction!" << std::endl;
        break;
    }
    if( IsInBorders( { newPosition.x, newPosition.y } ) )
        setPosition( newPosition );
    else
        std::cout << "ERROR:  Border reached! No movement possible!" << std::endl;
}


void PlayerC::AnimateMovement( void )
{
    static auto animationCounter = 0;
    static auto animationSpeedCounter = 0;

    if( !IsMoving() || !(animationCounter < Settings::animationTexturesSize) )
    {
        animationCounter = 0;
    }
    playerTexture->loadFromFile( Settings::animationTextures[animationCounter] );

    if( ++animationSpeedCounter > Settings::animationSpeed )
    {
        animationCounter++;
        animationSpeedCounter = 0;
    }
}


const bool PlayerC::IsInBorders( std::pair<float, float> requestedPosition ) const
{
    return IsInHorizontalBorder( requestedPosition.first ) && IsInVerticalBorder( requestedPosition.second );
}


const bool PlayerC::IsInVerticalBorder( const float requestedYPosition ) const
{
    std::cout << "PlayerC::IsInVerticalBorder( " << requestedYPosition << " )" << std::endl;
    return requestedYPosition >= 0.0f && requestedYPosition <= sf::VideoMode::getDesktopMode().height;
}


const bool PlayerC::IsInHorizontalBorder( const float requestedXPosition ) const
{
    std::cout << "PlayerC::IsInHorizontalBorder( " << requestedXPosition << " )" << std::endl;
    return requestedXPosition >= 0.0f && requestedXPosition <= sf::VideoMode::getDesktopMode().width;
}


void PlayerC::Rotate( const float angle )
{
    std::cout << "PlayerC::Rotate( " << angle << " )" << std::endl;
    rotate( angle );
}


const float PlayerC::GetAngle( void ) const
{
    return getRotation();
}


const bool PlayerC::IsMoving( void ) const
{
    return std::find_if( movementStatus.begin(), movementStatus.end(), [this]( bool status )->bool { return status == true; } ) != movementStatus.end();
}


const float PlayerC::DirectionToAngle( Movement::Directions&& direction ) const
{
    return static_cast<float>(90 * direction);
}
