#include "GameScreenC.hpp"
#include "Screens/Equipment/Models/Box.hpp"

#include <iostream>


GameScreenC::GameScreenC( void )
{
    std::cout << "GameScreenC::GameScreenC()" << std::endl;
    backgroundImage.loadFromFile( "Dependencies/Textures/Plain.png" );
    background.setTexture( &backgroundImage );

    player = std::make_shared<PlayerC>();
    AddWidget( player );
}


GameScreenC::~GameScreenC( void )
{
    std::cout << "GameScreen::~GameScreenC()" << std::endl;
}


void GameScreenC::ScreenControlStateMachine( sf::Event&& event )
{
    std::cout << "GameScreenC::ScreenControlStateMachine( " << &event << " )" << std::endl;
    if( event.type == sf::Event::KeyPressed )
    {
        switch( event.key.code )
        {
        case sf::Keyboard::A:
            player->MoveLeft();
            break;
        case sf::Keyboard::S:
            player->MoveDown();
            break;
        case sf::Keyboard::D:
            player->MoveRight();
            break;
        case sf::Keyboard::W:
            player->MoveUp();
            break;
        case sf::Keyboard::Key::E:
            player->Stop();
            BoxC::GetInstance()->SetPosition( {player->getPosition().x, player->getPosition().y} );
            SetActive( false );
            ScreensInfo::currentScreen = ScreensInfo::Tag::Equipment;
            break;
        case sf::Keyboard::Space:
            break;
        case sf::Keyboard::Key::Escape:
            SetActive( false );
            ScreensInfo::currentScreen = ScreensInfo::Tag::Menu;
            break;
        default:
            std::cout << "ERROR:  Unknown key code!" << std::endl;
            break;
        }
    }
    else if( event.type == sf::Event::KeyReleased )
    {
        switch( event.key.code )
        {
        case sf::Keyboard::A:
            player->Stop( Movement::Directions::LEFT );
            break;
        case sf::Keyboard::S:
            player->Stop( Movement::Directions::DOWN );
            break;
        case sf::Keyboard::D:
            player->Stop( Movement::Directions::RIGHT );
            break;
        case sf::Keyboard::W:
            player->Stop( Movement::Directions::UP );
            break;
        }
    }
}


void GameScreenC::StartGame( void )
{
    std::cout << "GameScreenC::StartGame()" << std::endl;
    isPaused = false;

    if( game.joinable() )
        game.join();

    game = std::move(std::thread( [this]()->void
    {
        while( !IsPaused() )
        {
            UpdateGame();

            std::this_thread::sleep_for( std::chrono::seconds( 2 ) );
        }
    } ));
}


void GameScreenC::PauseGame( void )
{
    std::cout << "GameScreenC::PauseGame()" << std::endl;
    isPaused = true;
    game.join();
}


const bool GameScreenC::IsPaused( void )
{
    return isPaused;
}


void GameScreenC::SetActive( const bool isActive )
{
    std::cout << "GameScreenC::SetActive( " << isActive << " )" << std::endl;
    isActive ? StartGame() : PauseGame();
    this->isActive = isActive;
}


void GameScreenC::UpdateGame( void )
{
    std::cout << "GameScreenC::UpdateGame()" << std::endl;
}
