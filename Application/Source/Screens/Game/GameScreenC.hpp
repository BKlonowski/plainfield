#pragma once

#include "Screens\Screens.hpp"
#include "Screens\Game\Models\Player\Player.hpp"


class GameScreenC : public ScreensInfo::Screen
{
public:
    GameScreenC( void );
    ~GameScreenC( void );

    void ScreenControlStateMachine( sf::Event&& event ) override;

    void StartGame( void );
    void PauseGame( void );

    const bool IsPaused( void );

    void SetActive( const bool isActive ) override;

private:
    void UpdateGame( void );

    bool isPaused;
    std::shared_ptr<PlayerC> player;

    std::thread game;
};
