#pragma once

#include <utility>
#include <string>


namespace Settings
{
    constexpr std::pair<float, float> playerSizeInPixels = { 50.f, 50.f };
    constexpr float gameFieldBorderWidth = 20.f;
    constexpr std::pair<float, float> menuArrowSizeInPixels = { 200.f,150.f };
    constexpr std::pair<float, float> equipmentBoxSize = { 400.f,50.f };

    constexpr int animationTexturesSize = 12;
    constexpr int animationSpeed = 5;
    static const std::string animationTextures[] =
    {
        "Dependencies/Textures/Player_stop.png",
        "Dependencies/Textures/Player_move_6.png",
        "Dependencies/Textures/Player_move_1.png",
        "Dependencies/Textures/Player_move_2.png",
        "Dependencies/Textures/Player_move_1.png",
        "Dependencies/Textures/Player_move_6.png",
        "Dependencies/Textures/Player_stop.png",
        "Dependencies/Textures/Player_move_5.png",
        "Dependencies/Textures/Player_move_3.png",
        "Dependencies/Textures/Player_move_4.png",
        "Dependencies/Textures/Player_move_3.png",
        "Dependencies/Textures/Player_move_5.png"
    };
}
