#include <SDKDDKVer.h>
#include "CppUnitTest.h"
#include "Screens/Game/Models/Player/Player.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
    TEST_CLASS( PlayerTest )
    {
    public:
        TEST_METHOD( SetPosition )
        {
            sf::Vector2f initPosition(sf::VideoMode::getDesktopMode().width / 2, sf::VideoMode::getDesktopMode().height / 2);
            player.SetPosition( initPosition.x, initPosition.y );
            int first = initPosition.x;
            int second = player.GetPosition().first;
            Assert::IsTrue( first == second );
        }

        TEST_METHOD( IsPlayerMovingTest )
        {
            sf::Vector2f initPosition( sf::VideoMode::getDesktopMode().width / 2, sf::VideoMode::getDesktopMode().height / 2 );
            player.SetPosition( initPosition.x,initPosition.y );
            player.MoveDown();
            Assert::IsTrue( player.IsMoving() );
            player.Stop(Movement::Directions::DOWN);
            Assert::IsFalse( player.IsMoving() );

            player.MoveRight();
            Assert::IsTrue( player.IsMoving() );
            player.Stop( Movement::Directions::RIGHT );
            Assert::IsFalse( player.IsMoving() );

            player.MoveUp();
            Assert::IsTrue( player.IsMoving() );
            player.Stop( Movement::Directions::UP );
            Assert::IsFalse( player.IsMoving() );

            player.MoveLeft();
            Assert::IsTrue( player.IsMoving() );
            player.Stop( Movement::Directions::LEFT );
            Assert::IsFalse( player.IsMoving() );
        }

        TEST_METHOD( MoveDownTest )
        {
            sf::Vector2f initPosition( sf::VideoMode::getDesktopMode().width / 2, sf::VideoMode::getDesktopMode().height / 2 );
            player.SetPosition( initPosition.x, initPosition.y );

            player.MoveDown();
            std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
            player.Stop( Movement::Directions::DOWN );
            Assert::IsTrue( initPosition.y < player.getPosition().y );
        }

        TEST_METHOD( MoveRightTest )
        {
            sf::Vector2f initPosition( sf::VideoMode::getDesktopMode().width / 2, sf::VideoMode::getDesktopMode().height / 2 );
            player.SetPosition( initPosition.x, initPosition.y );

            player.MoveRight();
            std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
            player.Stop( Movement::Directions::RIGHT );
            bool cond = initPosition.x < player.getPosition().x;
            Assert::IsTrue( cond );
        }

        TEST_METHOD( MoveUpTest )
        {
            sf::Vector2f initPosition( sf::VideoMode::getDesktopMode().width / 2, sf::VideoMode::getDesktopMode().height / 2 );
            player.SetPosition( initPosition.x, initPosition.y );
            
            player.MoveUp();
            std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
            player.Stop(Movement::Directions::UP);
            Assert::IsTrue( initPosition.y > player.getPosition().y );
        }

        TEST_METHOD( MoveLeftTest )
        {
            sf::Vector2f initPosition( sf::VideoMode::getDesktopMode().width / 2, sf::VideoMode::getDesktopMode().height / 2 );
            player.SetPosition( initPosition.x, initPosition.y );

            player.MoveLeft();
            std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
            player.Stop( Movement::Directions::LEFT );
            Assert::IsTrue( initPosition.x > player.getPosition().x );
        }

    private:
        PlayerC player;
    };
}