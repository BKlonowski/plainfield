#include <SDKDDKVer.h>
#include "CppUnitTest.h"
#include "Screens/Equipment/Models/Box.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
    TEST_CLASS( Box )
    {
    public:
        TEST_METHOD( GetInstanceTest )
        {
            auto box = BoxC::GetInstance();
            Assert::IsNotNull( &box );
        }
        TEST_METHOD( SetPosition )
        {
            std::pair<float,float> initPosition = { 100.f, 100.f };
            BoxC::GetInstance()->SetPosition( initPosition );
            Assert::IsTrue( BoxC::GetInstance()->GetPosition() == initPosition );
        }
    private:
    };
}
