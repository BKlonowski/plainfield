#include <SDKDDKVer.h>
#include "CppUnitTest.h"
#include "Screens/Equipment/Equipment.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
    TEST_CLASS(Equipment)
    {
    public:
        TEST_METHOD(IsOpenedTest)
        {
            Assert::IsFalse( equipment.IsOpened(), L"Equipment is opened at the beginning of the game!" );
        }
        TEST_METHOD(OpenTest)
        {
            equipment.Open();
            Assert::IsTrue( equipment.IsOpened() );
        }
    private:
        EquipmentC equipment;
	};
}
